/* global angular, define*/
define([], function () {
  /**
  * crudProvider Module
  *
  * Description
  */
  angular.module('ngCrudProvider', []).provider('crudSettings', [function () {
    this.rootUrl = '/static/apps/ngCrud/ngCrud/'
    this.mainUrl = this.rootUrl + 'templates/html/main.html'

    this.$get = [function () {
      return {
        rootUrl: this.rootUrl,
        mainUrl: this.mainUrl
      }
    }]

  }])
})
