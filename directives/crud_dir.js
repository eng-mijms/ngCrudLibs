/* global angular, define*/
define(['../controllers/crud_ctrl', '../providers/crud_provider'], function () {
  /**
  * crudDir Module
  *
  * Description
  */
  angular.module('ngCrudDir', ['ui.bootstrap', 'ngCrudCtrl']).directive('autoSearch', ['$http', 'crudSettings', function ($http, crudSettings) {
    return {
      restrict: 'EA',
      // replace: true,
      scope: {
        placeholder: '@placeholder',
        url: '@url',
        model: '=',
        limit: '@limitation',
        key: '@',
        value: '@',
        resultTemplate: '@',
        callback: '=',
        method: '@',
        delay: '@',
        require: '@',
        name: '@',
        multiple: '@',
        formatter: '=',
        cuitem: '=',
        field: '='
      },
      templateUrl: crudSettings.rootUrl + 'templates/html/search.html',
      controller: 'SearchCtrl as sc',
      link: function (scope, ele, attr) {
        // scope.selectItem = function ($item, model){
        //     debugger
        //     return scope.callback($item, model, scope.cuitem) || model
        //     // return out
        // }
      }
    }
  }]).directive('executionConfirm', ['$modal', 'crudSettings', function ($modal, crudSettings) {
    return {
      scope: {
        confirmFunc: '&',
        message: '@'
      },
      link: function (scope, element, attr) {
        //   var msg = attr.ngConfirmClick || "Are you sure?"
        element.bind('click', function (event) {
          scope.message = scope.message || 'Are you Sure to delete this item?'
          scope.confirmationInstance = $modal.open({
            animation: true,
            templateUrl: crudSettings.rootUrl + 'templates/html/ConfirmationPopup.html',
            scope: scope

          })
          scope.event = event
        })
      },
      controller: function ($scope) {
        $scope.ok = function () {
          $scope.confirmFunc()
          $scope.cancel()
        }
        $scope.cancel = function () {
          $scope.confirmationInstance.close()
        }
      }
      // templateUrl: 'App/Product/Templates/ConfirmationPopup.html'
    }
  }]).directive('compareTo', compareTo)

  function compareTo () {
    return {
      restrict: 'EA',
      require: 'ngModel',
      scope: {
        compare: '@',
        item: '='
      },
      link: function (scope, element, attributes, ngModel) {
        if (scope.compare) {
          // debugger
          ngModel.$validators.compareTo = function (modelValue) {
            if (!modelValue) {
              return true
            }
            return modelValue === scope.item[scope.compare]
          }
          var watched = 'item' + '.' + scope.compare
          scope.$watch(watched, function () {
              ngModel.$validate()
            })
        }
      }
    }
  }
})
