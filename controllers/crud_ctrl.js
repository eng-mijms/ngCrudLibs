/* global angular, define*/
define(['../services/crud_serv'], function () {
  /**
  * ngCrudCtrl Module
  *
  * Description
  */
  angular.module('ngCrudCtrl', ['ngCrudServ', 'ui.router', 'ui.bootstrap', 'infinite-scroll'])
  .controller('CrudCtrl', ['$scope', '$rootScope', 'defaultServ', 'crudServ', '$stateParams', 'EditPopupServ', '$filter', crudCtrl])
  .controller('SearchCtrl', ['$scope', 'defaultServ', '$http', '$filter', 'crudServ', searchCtrl])

  function crudCtrl ($scope, $rootScope, defaultServ, crudServ, $stateParams, EditPopupServ) {
    var cu = this
    cu.settings = $stateParams.settings
    // debugger
    cu.item = {}
    var next = cu.settings.url
    cu.list = []
    var listLoading = true
    cu.lists = crudServ.lists
    cu.crudServ = crudServ
    // crudServ.crudResource(cu.settings.url).query(function (queryset){
    //     cu.list = cu.list.concat(queryset.results)
    //     // cu.list = cu.list.concat(queryset.results)
    //     // debugger
    //     next = queryset.next
    //     listLoading=true
    // })
    cu.createSubmit = function (item, preSubmit, postSubmit) {
      // debugger
      if (preSubmit) {
        preSubmit(item)
      }
      console.log(item)
      crudServ.crudResource(cu.settings.url).save(item, function (responce) {
        cu.item = {}
        cu.list.push(responce)
        cu.modelActive = true
        if (postSubmit) {
          postSubmit(responce, cu.settings.fields)
        }
      }, function (responce) {
        console.error('error creating item')
      })
    }

    cu.getSelectedData = function (data, listName, url) {
      return crudServ.getSelectedData(data, listName, url)
    }

    cu.delete = function (item) {
      console.log(item)
      var index = cu.list.indexOf(item)
      crudServ.crudResource(cu.settings.url).delete({id: item.id}).$promise.then(
                function (responce) {
                  cu.list.splice(index, 1)
                }, function (responce) {
                  console.log('deleting error')
                })
      // cu.list.splice(index,1)
    }

    cu.openEditItemModal = function (item) {
      var index = cu.list.indexOf(item)
      EditPopupServ.servActions.openPopup(item, cu.settings.fields, 'Edit ' + cu.settings.title).result.then(function (selectedItem) {
        // debugger
        crudServ.crudResource(cu.settings.url).update({id: item.id}, selectedItem).$promise.then(
                    function (responce) {
                      cu.list[index] = responce
                    }, function (responce) {
                      console.log('updating Error')
                    })
        // cu.list[index] = selectedItem
      }, function () {
        console.log('Edit Modal dismissed at: ')
      })
    }

    cu.watchModelValid = function (field, form) {
      // debugger
      $scope.$watch('cu.item.' + field.model + '.length', function (viewValue) {
        // debugger
        try {
          if (!viewValue) {
            form[field.label].$setValidity('required', false)
          }else {
            form[field.label].$setValidity('required', true)
          }
        } catch (e) {
          form[field.label].$setValidity('required', false)
        }
      })
    }

    cu.removeFromList = function (index, list) {
      list.splice(index, 1)
    }
    cu.test = function (test) { console.log(test) }
    cu.getNext = function () {
      if (next && listLoading) {
        // debugger
        // var nextUrl = url || next
        listLoading = false
        // console.log('scroll function')
        crudServ.crudResource(next).query(function (queryset) {
          cu.list = cu.list.concat(queryset.results)
          next = queryset.next
          listLoading = true
        })
      }
    }

    if (cu.settings.permissions) {
      // debugger
      if (cu.settings.permissions.get($rootScope.User)) {
        cu.modelActive = true
        cu.getNext()
      }
    }else {
      cu.getNext()
    }
    cu.popupServ = EditPopupServ
  }

  // The search fuction contrlloer
  // params the service name , http , filter
  // output:return to the user the search data
  function searchCtrl ($scope, defaultServ, $http, $filter, crudServ) {
    var sc = this
    // debugger
    sc.crudServ = crudServ
    if ($scope.multiple) {
      // debugger
      sc.selectedList = []
      sc.results = []
      $scope.model = []
      $scope.callback = function (item, model) {
        // debugger
        sc.selectedList.push(angular.copy(item[$scope.value]))
        sc.results.push(angular.copy(item[$scope.key]))
        // debugger
        $scope.model = sc.results
      }
      sc.removeFromList = function (index) {
        // debugger
        sc.selectedList.splice(index, 1)
        sc.result.splice(index, 1)
      }
    }

    sc.formatLabel = function (model, val, key) {
      // console.log(searchData)
      if ($scope.multiple) {
        return
      }
      var out
      sc.searchResult.forEach(function (value) {
        // debugger
        var tempCondition = key ? value[key] : value
        if (tempCondition === model) {
          // debugger
          out = value[val]
          return
        }
      })
      // debugger
      return out
    }
    sc.searchedList = []
    sc.searchResult = []
    sc.getSearch = function (val, url, limitation, method, key) {
      // debugger
      // console.log(sc.searchedList.indexOf(val))
      if (sc.searchedList.indexOf(val) === -1) {
        if (method === 'static') {
          if (sc.searchResult.length > 0) {
            return $filter('limitTo')($filter('filter')(sc.searchResult, val), limitation)
          }
        }
        if ($filter('filter')(sc.searchResult, val).length < limitation) {
          var request = method === 'post' ? $http.post(url, {q: val}) : $http.get(url, {params: {q: val}})

          return request.then(function (response) {
            sc.searchedList.push(val)
            angular.forEach(response.data, function (data) {
              if ($filter('filter')(sc.searchResult, data[key]).length === 0) {
                // debugger
                sc.searchResult.push(data)
              }
            })
            return $filter('limitTo')($filter('filter')(sc.searchResult, val), limitation)
          })
        } else {
          return $filter('limitTo')($filter('filter')(sc.searchResult, val), limitation)
        }
      } else {
        return $filter('limitTo')($filter('filter')(sc.searchResult, val), limitation)
      }
    }
  }
})
