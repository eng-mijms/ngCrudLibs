/* global angular, define*/
define(['./controllers/crud_ctrl', './services/crud_serv', './providers/crud_provider', './directives/crud_dir'], function () {
  /**
  * ngCrud Module
  *
  * Description
  */
  angular.module('ngCrud', ['ngCrudCtrl', 'ngCrudProvider', 'ngCrudDir'])
})
