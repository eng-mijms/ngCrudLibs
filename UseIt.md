# ngCrud
## ngCrud Use Documentation

## Introduction

### ngCrud
stands for [Angular](https://angularjs.org/) crud (create, read, update, delete) operations. This documentation demonstrate for developers how can use it.

## Dependencies
 - [Angular](https://angularjs.org/)
 - [Ui Router](https://github.com/angular-ui/ui-router)
 - [Ui Bootstrap](https://angular-ui.github.io/bootstrap/)
 - [Require Js](http://requirejs.org/)
 - [Infinite Scroll Modified](https://sroze.github.io/ngInfiniteScroll/)


### Libraries modification
in Infinite scroll library From line 32 until scope.$on('$destroy')

```javascript
windowBottom = $window[0].innerHeight + $window[0].pageYOffset;
elementBottom = elem[0].scrollTop + elem[0].offsetHeight;
remaining = elementBottom - windowBottom;
shouldScroll = remaining <= $window[0].innerHeight * scrollDistance;
if (shouldScroll && scrollEnabled) {
    if ($rootScope.$$phase) {
      return scope.$eval(attrs.infiniteScroll);
    } else {
      return scope.$apply(attrs.infiniteScroll);
    }
} else if (shouldScroll) {
    return checkWhenEnabled = true;
}
};
$window.on('scroll', handler);
```

## Installing
```
git clone https://gitlab.com/eng-mijms/ngCrudLibs.git
```

## Running
### loading library
create a loading js file to include ngCrudFolder inside it by Using [Require Js](http://requirejs.org/)
```javascript
require(['./app'], function(){
//your code go Here



//bootstrap your angular application like that
//replacing ng-app="myApp"
//with
    angular.bootstrap(document.body, ['myApp']); // ng-app="myApp"
});
```

in your index.html include this loading file
```html
<script type="text/javascript" data-main="/{yourApp}/loading.js" src="{path to}/require.min.js"></script>
```

## Provider Settings
You can set root Folder and customize main if you want it your design
how can you get things inside your customized main please refer to dev Documentation

#### crudSettingsProvider Settings
```javascript
angular.module('test', ['ui.router', 'ngCrud']).config(['$stateProvider','$resourceProvider', 'crudSettingsProvider', function ($stateProvider, $resourceProvider, crudSettingsProvider ) {
//crudSettingsProvider is our provider can set two params
//rootUrl this is default '/static/apps/ngCrud/'
//main is the template main.html file position default is '/static/apps/ngCrud/templates/html/main.html'
    $resourceProvider.defaults.stripTrailingSlashes = false;
    var main = crudSettingsProvider.mainUrl;

    $stateProvider.state('home',{
        url:'',
        controller: 'CrudCtrl as cu',
        templateUrl: main,
        params:{settings:setting}
    })
}]);
```
##### Params
###### rootUrl
is ngCrud url default is '/static/apps/ngCrud/'

###### mainUrl
is ngCrud main template url default is '/static/apps/ngCrud/templates/html/main.html'
###### params
```javascript
params: {settings:setting}
```
send params to library object please refere to library setting for more details

## Params Settings
Params Settings Variable is a JavaScript Object that contain keys

```javascript
var setting = {
    title:"User", //title of crud app (list User, create User)
    index:"Num", //show index (record Number) in list missing = no index
    url: '/inventory/api/v1/products/:id/', //deal with REST API url standard in backend
    views:[                 //list of objects to view in table

        //view object can contain elements (head, view, expression)
        {
            head:"name",        //head in table <th> </th>
            view:"fullname",    //point to retrived data object say user.fullname
        },{
            head:"ID",
            view:"id",
        },
        {
            head:"Email",
            view:"email",
        },
        {
            head:"ID+100",
            expression: function(item){ //expresion is callback function to execute
                return 7;
            },
        }
    ],
    fields:[                //list of objects mapped to create form fields
    //one object can contain (label, type, placeholder, model, required, .....etc(coressponding to field type))
    {
        label: "User Name", // will show in label
        type: "text", //field type
        placeholder: "Please Enter your Prefered user name",//placeholder inside field
        model: "username",//ng-model to send to server
        required:true,//ng validation missing = false

    },
    {
        label: "password",
        type: "password",
        // label: "User Name",
        placeholder: "Please Enter your Password",
        model: "password",
    },
    {
        label: "Description",
        type: "textarea",
        row: 10,    // row for textarea default if not sended is 30
        col: 9,     // col for textarea default if not sended is 3
        placeholder: "Please Enter User Description",
        model: "desc",
        required:true,

    },
    {
        label: "Group",
        type: "select",
        placeholder: "Please Enter User Group",
        model: "group_id",
        listName: 'group',
        data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}], //data shown in select
        key: "id", //key selected in ng-model
        value: "name",  //name to shown to user
        placeholderEnable: true, //enable to chooce placeholder == undefined value
        required:true,

    },{
        label: "Product",
        type: "select",
        placeholder: "Please Enter Product",
        model: "group_id",
        url: '/inventory/api/v1/products/', //url to get list allow to pagnition under REST API standars
        listName: 'product',//stored list in memory for unrepeat data fetching
        key: "id",
        value: "name_eng",
        placeholderEnable: true,
        required:true,

    },{
        label: "Product",
        type: "select",
        placeholder: "Please Enter Product",
        model: "group_id",
        url: '/inventory/api/v1/products/',
        listName: 'product',
        key: "id",
        value: "name_eng",
        placeholderEnable: true,
    },{
        label: "user",
        type: "select",
        placeholder: "Please Enter User",
        model: "group_id",
        url: '/auth/api/v1/users/',
        listName: 'user',
        key: "id",
        value: "email",
        placeholderEnable: true,
    },
    {
        label: "Search test",
        type: "search",
        placeholder: "Please Enter Searched text",
        model: "group",
        url: '/static/countrydata.json',//search url default method get with q="searched string"
        key: "code",
        value: "name",
        required:true,
        limit:4, //limit viewed result default if not set 10
    },{
        label: "Search",
        type: "search",
        placeholder: "Please Enter Searched text",
        model: "group2",
        url: '/static/countrydata.json',
        // data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}],
        key: "name",
        value: "code",
        resultTemplateUrl: '/static/apps/ngCrud/templates/html/search_result.html', //customize result template to viewed
        limit:4,
        onSelect: function(item, model){ // call this function on search select
            debugger
            model = "ahmed";
            return model;
        },
        delay:1000,//delay when user type to send request to url 3000 millisecond
    },
    ]
};
```


#### title
Set the Grid title

#### url
Server Url to deal with Crud operations according to
say your server url for model is
http://example.com/model/:id/
```
CRUD OPERATIONS


    list reading
    GET http://example.com/model/
    and data inside result json object

    create new
    Post http://example.com/model/

    read First model
    Get http://example.com/model/1/

    update First model
    PUT http://example.com/model/1/

    delete First
    DELETE http://example.com/model/1/
```

### index
index is enable table numbering and head is the value

## views
is list variable that contain the grid title is presented by head and value is expression or view
```javascript
//view full_name of model under grid title
var view = {
    head: 'title',
    view: 'full_name'
}

//view object name inside item
var view = {
    head: 'title',
    expression: function (item) {
        return item.object.name
    }
}
```

## fields
send list of fields to create and update through it
### text
```javascript
var field = {
    label: "User Name", // will show in label
    type: "text", //field type
    placeholder: "Please Enter your Prefered user name",//placeholder inside field
    model: "username",//ng-model to send to server
    required:true,//ng validation missing = false
    edit: true  //enable in edit
}
```

### password
```javascript
var field = {
    label: "Pass Word", // will show in label
    type: "password", //field type
    placeholder: "Please Enter your Prefered Password",//placeholder inside field
    model: "password",//ng-model to send to server
    required:true,//ng validation missing = false
    edit: true  //enable in edit
}
```

### number
```javascript
var field = {
    label: "Age", // will show in label
    type: "number", //field type
    placeholder: "Please Enter your age",//placeholder inside field
    model: "age",//ng-model to send to server
    required:true,//ng validation missing = false
    edit: true  //enable in edit
}
```

### compareTo
```javascript
var field = {
    label: "Confirm Pass Word", // will show in label
    type: "password", //field type
    placeholder: "Please Enter your Prefered Password",//placeholder inside field
    compareTo: "password", //compare to this model
    model: "confirm_password",//ng-model to send to server
    required:true,//ng validation missing = false
    edit: true  //enable in edit
}
```

### textarea
```javascript
var field = {
    label: "Desc", // will show in label
    type: "textarea", //field type
    placeholder: "Please Enter Description here",//placeholder inside field
    model: "desc",//ng-model to send to server
    required:true,//ng validation missing = false
    edit: true  //enable in edit
}
```

### select
#### select with static data
```javascript
var field = {
    label: "Group",
    type: "select",
    placeholder: "Please Enter User Group",
    model: "group_id",
    data: [{id:1,name:"test"},{id:2,name:"test 2"},{id:3,name:"test 3"},{id:4,name:"test 4"}], //data shown in select
    key: "id", //key selected in ng-model
    value: "name",  //name to shown to user
    placeholderEnable: true, //enable to choose placeholder == undefined value
    required:true,

}
```

#### select from url
```javascript
var field = {
    label: "Group",
    type: "select",
    placeholder: "Please Enter User Group",
    url: '/auth/api/v1/groups/' // take data from url
    model: "group_id",
    listName: 'group', // listname not to reload the data
    key: "id", //key selected in ng-model
    value: "name",  //name to shown to user
    placeholderEnable: true, //enable to choose placeholder == undefined value
    required:true,

}
```

#### Multiple
```javascript
var field = {
    label: "Group",
    multiple: true, //allow multi selection
    selectSize: 20, //set selection size
    type: "select",
    placeholder: "Please Enter User Group",
    url: '/auth/api/v1/groups/' // take data from url
    model: "group_id",
    listName: 'group', // listname not to reload the data
    key: "id", //key selected in ng-model
    value: "name",  //name to shown to user
    placeholderEnable: true, //enable to choose placeholder == undefined value
    required:true,

}
```

### Search
```javascript
var field = {
  label: 'Country',
  type: 'search',
  multiple: true, // allow multi selection
  url: '/static/data/country.json',
  key: 'name', //key selected in ng-model
  value: 'name', //name to shown to user
  placeholder: 'Please search in Countries',
  model: 'country',
  method: 'static', // Search Method (static(files)- get- post)
  required: true,
  delay: 10, //delay default 300 ms
  edit: true,
  formatter: function (model, searchResult) {
    //model selected item from search result
    //searchResult is search Result array contain all search items


    //return the format after selection to put into search text box
  },
  onSelect: function (item, model, fields, cuitem, crudServ) {
    // calling when item is selected
    //item == selected item
    //model == selected item key
    //fields == field settings to create and update item
    //cuitem === created item that will send to backend
    //crudServ === the crud Using service
  },
}
```

#### crudServ
crud Service elements
```javascript
crudResource,
lists,
getAbsluoteUrl

crudServ.crudResource(url) //crud operation according to ngResource on this url
crudServ.lists = []  //lists of loaded data for selection
crudServ.getAbsluoteUrl(url, Data) //send get request to this url with that data Data
```

### field extracts
add an extraction buttons to one fields
> type
array

```javascript
var field = label: 'Comment',
type: 'textarea',
placeholder: 'Please Enter Comment Here',
model: 'comment',
edit: true,
extracts: [
    {
  text: 'Create New Product', // text appear inside button
  class: 'btn-warning', // button class to add to this button
  permission: function (User) {}, //permission function  to this button
      //crudserv as before
  action: function (popupServ, crudServ) {
    // popupServ.servActions.openPopup({}, productSettings.fields, 'Create New Product', true).result.then(
    //               function (result) {
    //                 try {
    //                   crudServ.crudResource(productSettings.url).save(result,
    //                       function (result) {}
    //                   )
    //                 } catch (e) {}
    //               },
    //               function () {
    //                 console.log('Create new Product is canceled')
    //               }
    //           )
    //   }
    }
  ]
```

#### popupServ variables

```javascript
servActions.openPopup(object, fields, title(optional), type(optional)) // launch pop up with object and fields with title given if type=true will take all the fields not will take editable fields only
servActions.openConfirmPopUp(item, msg, title) // make confirmation msg with this msg on this title and passing this item finally in result
```

popUp result under [Ui bootstrap Modal](https://angular-ui.github.io/bootstrap/)

## list type
```javascript
var field = {
  label: 'Materials',
  type: 'list',
  show: function (item) {
    //  debugger
    return item.material.name
  }, // return the showed value to be shown for one item in length
  multiple: true,
  options: [{
    class: 'btn-primary',
    text: 'add Material',
    permission: function (User) {
      return true
    },// button permission function
    action: function (item, popupServ) {
        //   debugger
        popupServ.servActions.openPopup({}, popUps.materials, 'Add New Material', true).result.then(
                  function (result) {
                    //  result is popup saved item
                    try {
                      item.materials.push(result) // push to materials if exist
                    } catch (e) {
                      item.materials = [] // create the material
                      item.materials.push(result)
                    }
                  },
                  function () { // function called when cancel popup
                    console.log('add new item is canceled')
                  }
              )
    }],
    url: 'url',
    listName: 'material',
    key: 'id',
    value: 'name',
    model: 'materials',
    edit: true
}
```

## Submit
```javascript
var option = {
    preSubmit: function (item) { item.saved = true }, //call before submit
    postSubmit: function (responce, fields) {}, //call after submission complete send responce and fields as inputs
    submit: function (item, list, crudServ, fields) { } // override submit function method
}
```
## options
same as extracts but for listed items

```javascript
var option = options: [
  {
    text: 'Change Password',
    class: 'btn-warning',
    permission: function (User) { // button permission
    //   return userSettings.permissions.update(User)
    },
    action: function (item, popupServ, crudServ, list) {
      var index = list.indexOf(item)
      // debugger
      popupServ.servActions.openPopup(item, changePasswordFields, 'change User Password', true).result.then(function (selectedItem) {
        // debugger
        crudServ.crudResource(userSettings.url + 'set_password/').update({id: item.id}, selectedItem).$promise.then(
                    function (responce) {
                      list[index] = responce
                    }, function (responce) {
                      console.log('updating Error')
                    })
        // list[index] = selectedItem
      }, function () {
        console.log('Edit Modal dismissed at: ')
      })
    }
  }
],
```

## extend
same as extracts but for created form
```javascript
extend: [
  {
    text: 'Save Draft',
    class: 'btn-primary',
    validate: true,
    action: function (item, crudServ, list, cu) {
      // debugger
      var url = billSettings.url
      crudServ.crudResource(url).save(item, function (responce) {
        item = {}
        list.push(responce)
        cu.modelActive = true
      }, function (responce) {
        console.error('error saving bill item')
      })
    }
  }
]
```
## permissions
permission for get, update, delete, create
```javascript
permissions: {
  model: 'bill',
  get: function (User) { //get permission
    return permission.get(this.model, User)
  },
  update: function (User, item) { // update permission
    try {
      if (item.saved) {
        return false
      }
    } catch (e) { }
    return permission.update(this.model, User)
  },
  delete: function (User, item) { //delete permission
    try {
      if (item.saved) {
        return false
      }
    } catch (e) { }

    return permission.delete(this.model, User)
  },
  create: function (User) { //create permission
    return permission.create(this.model, User)
  },
  all: function (User) { // all permission
    // debugger
    return permission.get(this.model, User) || permission.update(this.model, User) || permission.delete(this.model, User) || permission.create(this.model, User)
  }
}
```
